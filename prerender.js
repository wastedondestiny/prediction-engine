const fs = require('fs').promises
const express = require('express');
const puppeteer = require('puppeteer');

(async () => {
  // Create a simple Express server that prints "Hello, World"
  const app = express();
  app.use(express.static('./dist'));
  const server = await app.listen(3000);
  
  // Launch Puppeteer and navigate to the Express server
  const browser = await puppeteer.launch({ slowMo: 500 });
  const page = await browser.newPage();
  await page.goto('http://localhost:3000');
  
  const content = await page.content();
  await fs.writeFile('./dist/index.html', content);
  
  // Cleanup 
  await browser.close();
  await server.close();
})()
